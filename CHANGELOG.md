# CKEditor 4 Angular Integration Changelog

## ckeditor4-angular 1.0.0

New Features:

* [#6](https://github.com/ckeditor/ckeditor4-angular/issues/6): Added support for Classic (iframe-based) Editor. Starting from this version Classic Editor is used by default.
* [#40](https://github.com/ckeditor/ckeditor4-angular/pull/40): Added support for Angular 5.

Fixed Issues:

* [#42](https://github.com/ckeditor/ckeditor4-angular/issues/42): Fixed: The `elementRef` related error is thrown when using Angular 5.
* [#54](https://github.com/ckeditor/ckeditor4-angular/issues/54): Fixed: Two-way data binding does not work when [Undo](https://ckeditor.com/cke4/addon/undo) plugin is not present.

Other Changes:

* Updated default CDN CKEditor 4 dependency to [4.13.0](https://github.com/ckeditor/ckeditor4-angular/issues/59).

## ckeditor4-angular 1.0.0-beta.2

Other Changes:

* Updated default CDN CKEditor 4 dependency to [4.12.1](https://github.com/ckeditor/ckeditor4-angular/commit/2bf8a8c489f2a9ea2f2d9304e2e3d92646dbe89e).

## ckeditor4-angular 1.0.0-beta

Other Changes:

* [#28](https://github.com/ckeditor/ckeditor4-angular/issues/28): Updated package dev dependencies. 

## ckeditor4-angular 0.1.2

Other Changes:

* [#20](https://github.com/ckeditor/ckeditor4-angular/issues/20): Added "Quick Start" section to README file.
* [#10](https://github.com/ckeditor/ckeditor4-angular/issues/10): Updated LICENSE file with all required dependencies.

## ckeditor4-angular 0.1.1

Other Changes:

* `README.md` file improvements.

## ckeditor4-angular 0.1.0

The first beta release of CKEditor 4 WYSIWYG Editor Angular Integration.
